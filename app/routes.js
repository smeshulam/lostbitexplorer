// require express
const express = require('express');
const path    = require('path');
const getJSON = require('get-json');

// create our router object
var router = express.Router();

// export our router
module.exports = router;

// route for the homepage
router.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/index.html'));
});

// route for the dashboard
router.get('/dashboard', function(req, res) {
  res.sendFile(path.join(__dirname, '../public/dashboard.html'));
});

router.get('/api', (req, res) => {
  res.setHeader('Content-Type', 'application/json');
  const bitcoinAddress = req.param('a');
  getJSON(`https://blockchain.info/rawaddr/${bitcoinAddress}`, (error, response) => {
    console.log(bitcoinAddress, response)
    res.send(error || convertData(response));
  });

  function convertData(data) {
    console.log(data)
    const txs = [];
    //for each all transactions
    data.txs.forEach((tx) => {
      //set total to 0
      let total = 0;
      //
      let type;
      //loop all out addresses from curent transaction
      tx.out.forEach((out) => {
        //look if address belongs to the beeing and push transaction to transaction array
        if(out.addr === data.address){
          type = 'green';
          total += out.value;
        }
      });
      tx.inputs.forEach((input) => {
        //look if address belongs to the beeing and push transaction to transaction array
        if(input.prev_out.addr === data.address){ 
          type = 'yellow';
          total += input.prev_out.value;
        }
      });
      //push transaction details to transaction array
      txs.push({
              'id': tx.tx_index,
              'hash': tx.hash,
              'start': new Date(tx.time * 1000),
              'timestamp': tx.time,
              'value': total,
              'className': type,
              'from': tx.inputs ,
              'to': tx.out,
              })
      });

      return txs;
  };
});
	