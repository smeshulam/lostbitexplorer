import Modal from '../modules/modal.js';
import Case from '../modules/case.js';
import List from '../modules/list.js';
import Network from '../modules/network.js';
import TimeLine from '../modules/timeline.js';

export default class{
    constructor(elementId, caseInstance){
        this.element = document.getElementById(elementId);
        this.case =  new Case(caseInstance.name, caseInstance.addresses)
        this.addressesList = this.element.querySelector('.addresses-case');
        this.modal = this.modal = new Modal('modal');
        //
        this.bindEvents();
        this.init();
        this.draw();
    }

    init() {
         //draw the timeline component
        this.transactionstimeLine = new TimeLine('mytimeline', this.case.addresses[0].address);
        this.transactionstimeLine.draw(this.case.addresses[0].txs);
        //draw the transactions list component
        this.transactionsList = new List('transactions', this.case.addresses[0].address, this.case.addresses[0].color )
        this.transactionsList.draw(this.case.addresses[0].txs);
        //draw transaction network
        const treeData = {'name':this.case.addresses[0].address, 'children':this.case.addresses[0].txs.reverse()}
        this.transactionsNetwork = new Network('network', this.case.addresses[0].address, this)
        this.transactionsNetwork.draw(treeData);
    }

    bindEvents() {
        this.element.querySelector('.add-address').addEventListener('click', ()=> {
            this.modal.show("addAddress.html", this.addAddress.bind(this));
        })
    }

    draw() {
        this.element.querySelector('.case-name').innerHTML = this.case.name;
        const addressesList = () => {
            let html = '';
            this.case.addresses.forEach((address) => {
                html += `<li>${address.address}</li>`;
            })
            return html;
        }
        this.addressesList.innerHTML = addressesList(); 
    }

    addAddress(data) {
        this.case.addAddress(data.address);
        this.draw();
    }

    controller(trxId, caller) {
        trxId -= 1;
        switch(caller){
            case "Network":
                //if node with time is clicked
                if(this.case.addresses[0].txs[trxId].start) {
                    this.transactionstimeLine.adjustVisibleTimeRange(this.case.addresses[0].txs[trxId].start);
                    this.transactionsList.updateListSelection(this.case.addresses[0].txs[trxId].id);
                }
            break; 
            case "TimeLine":
                //if node with time is clicked
                if(this.case.addresses[0].txs[trxId].start) {
                    this.transactionsList.updateListSelection(this.case.addresses[0].txs[trxId].id);
                }
            break; 
            case "List":
                //if node with time is clicked
                if(this.case.addresses[0].txs[trxId].start) {
                    this.transactionstimeLine.adjustVisibleTimeRange(this.case.addresses[0].txs[trxId].start);
                }
            break;
       }
    }

}