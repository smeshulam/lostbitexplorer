import Modal from '../modules/modal.js';
import Case from '../modules/case.js';

export default class{
    constructor(elementId) {
        this.element = document.getElementById(elementId);
        this.cases = JSON.parse(localStorage.getItem('cases'));
        this.modal = new Modal('modal');

        if(!this.cases){
            localStorage.setItem('cases', JSON.stringify('[]'));
            this.cases = [];
        }

        this.draw();
        this.bindEvents();
    }

    draw() {
        //this draw blank case for adding cases 
        this.element.innerHTML = ` <div class="case case-empty">
                                        <span class="add">Add</span>
                                        <div class="add-symbol">
                                            <span>+</span>
                                        </div>
                                    </div>`;
        if(this.cases) {
            let caseHtml = '';
            this.cases.forEach(caseObject => {

                caseHtml += `<div class="case">
                                <ul>
                                    <li>
                                        <span class="case-name">${caseObject.name}</span>
                                        
                                        <ul>`;
                caseObject.addresses.forEach (address => {
                    caseHtml += `           <li>
                                                ${address.address}
                                            </li>`
                }) 
                caseHtml += `            </ul>
                                    </li>
                                </ul>
                            </div>`;    
            });
            this.element.innerHTML += caseHtml;
        }
    }

    showAddCaseModal() {
        this.modal.show("addCase.html", this.addCase.bind(this));
    }

    bindEvents() {
        const addCaseBtn = this.element.querySelector('.case-empty');
        const cases = Array.from(this.element.getElementsByClassName('case'));
        cases.splice(0,1);
        addCaseBtn.addEventListener('click', () => {
            this.showAddCaseModal();
        });
        cases.forEach((caseElement, i) => {
            caseElement.addEventListener('click', () => this.openCase(i));
        });
        
    }

    addCase(data) {
        fetch(`/api?a=${data.address}`, {
            method: 'get'
        }).then((response) => {
            return response.json();
        }).then((responseJson) => {
            const newCase = new Case(data.caseName, [{address: data.address, color:data.color,txs: responseJson}])
            this.cases.push(newCase);
            localStorage.setItem('cases', JSON.stringify(this.cases))
            this.draw();
            this.bindEvents()
        });
    }

    removeCase() {
    }

    openCase(index) {
        window.location = `dashboard?c=${index}`;
    }
}
