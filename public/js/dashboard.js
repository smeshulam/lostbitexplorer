import CaseEditor from './components/caseEditor.js'

init();
    
function init() {
    const url = new URL(window.location.href);
    const caseIndex = url.searchParams.get("c");
    const casesDatabase = JSON.parse(localStorage.getItem('cases'));
    const currentCase = casesDatabase[caseIndex];
    const addr = currentCase.addresses[0].address // set default view to first address

    //fix date objects after parse...
    currentCase.addresses[0].txs.forEach(transaction => {
        transaction.start = new Date(transaction.timestamp * 1000);
    }); 

    //draw the case editor
    const caseEditor = new CaseEditor('case-editor', currentCase)
}
