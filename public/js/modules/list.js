export default class List {
    constructor(elementId, adrr, color) {
        this.element = document.getElementById(elementId)
        this.addr = adrr;
        this.color = color;
    }

    bindEvents(){
        
    }
    
    //draw the list of transactions
    draw(txs) {
        this.element.innerHTML = '';
        txs.forEach((n) => {
            let fromTransactions ='';
            let toTransactions ='';
            let type;
            for(const tx in n.from){
                if(n.from[tx].prev_out.addr === this.addr) {
                    fromTransactions += `<p><span style="background-color:${this.color}">${n.from[tx].prev_out.addr}</span>`; // ${n[0].from[tx]
                }else{
                    fromTransactions += `<p><span>${n.from[tx].prev_out.addr}</span>`; // ${n[0].from[tx]
                }
               
                fromTransactions += `<span class="transaction-value">${n.from[tx].prev_out.value}</span></p>`
                if(!type && n.from[tx].prev_out.addr === this.addr){
                    type = 'from';
                }
            };
            
            for(const tx in n.to){
                if(n.to[tx].addr === this.addr) {
                    toTransactions += `<p><span style="background-color:${this.color}">${n.to[tx].addr}</span>`; 
                }else{
                toTransactions += `<p><span>${n.to[tx].addr}</span>`;
                }
                toTransactions += `<span class="transaction-value">${n.to[tx].value}</span></p>`;
                if(!type && n.to[tx].addr === this.addr){
                        type = 'to';
                    }
            };
    
            let trx = `<li id="${n.id}">
                    <table>
                        <tbody>
                            <tr>
                                <td class="transaction-header" colspan="3">
                                    <p>${n.start}</p>
                                </td>
                            </tr>
                            <tr>
                                <td class="transaction-from">
                                    ${fromTransactions}
                                </td>
                                <td class="transaction-movment">`

            if(type === 'to'){
                trx +=              `<img src="../img/arrow_right_green.png"/>`;                   
            }else if(type === 'from'){
                trx +=              `<img src="../img/arrow_right_red.png"/>`;          
            }
            
            trx +=             `</td>
                                <td class="transaction-to">
                                    ${toTransactions}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </li>`;    
                this.element.innerHTML += trx;
        })
    }
    
    updateListSelection(index) {
        const targetLi = this.element.querySelector('#\\3'+ String(index).substring(0, 1) + ' ' + String(index).substring(1, String(index).length)); //<li> element
        this.element.scrollTop = (targetLi.offsetTop - this.element.offsetTop);
        if(this.element.querySelector('li.selected')){
            this.element.querySelector('li.selected').classList.remove('selected');
        }
        targetLi.classList.add('selected');
    };

    click(listItem) {
        controller(d, /*index of li item*/);
    }
}