export default class{
    constructor(elementId){
        this.element = document.getElementById(elementId);
        this.template = this.element.querySelector('.template');
        this.closeBtn = this.element.querySelector('.close');

        this.closeBtn.onclick = () => {
            this.element.style.display = "none";
        }
        window.onclick = event => {
            if (event.target === this.element) {
                this.element.style.display = "none";
            }
        }
    }

    bindSubmit(callback){
        const form = this.template.contentDocument.querySelector('form');
        const formFields = Array.from(form.getElementsByTagName('input'));

        form.addEventListener('submit', () => {
            let data = {};

            formFields.forEach((input) => {
                if(input.getAttribute('type') != 'submit') {
                    data[input.getAttribute('name')] = input.value;
                }
            })

            callback(data)
            this.element.style.display = "none";
            });
    }

    show(htmlFileName, callback) {
        this.element.style.display = "block";
        this.template.style.visibility = null;
        this.template.setAttribute('data', `/templates/modal/${htmlFileName}`);
        this.template.style.visibility = 'visible';
        this.template.onload = () => this.bindSubmit(callback);
    }  
}