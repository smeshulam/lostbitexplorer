export default class Network{
    //todo programaticly style circles
    constructor(elementId, addr, controllerClass) {
        this.addr = addr;
        this.element = document.getElementById(elementId);
        this.controller = controllerClass;
    }
    
    //Called when the Visualization API is loaded.
    draw(treeData) {
        const boundingRect = this.element.getBoundingClientRect();
        const margin = {top: 20, right: 120, bottom: 20, left: 120},
            width = boundingRect.width - margin.right - margin.left,
            height = boundingRect.height - margin.top - margin.bottom;
        //zoom handler
        const zoom = d3.zoom()
            .on("zoom", function () {
                g.attr("transform", d3.event.transform)
            });
        //resize handler
        const resize = () => {
            var width = window.innerWidth, height = window.innerHeight;
            force.size([width, height]).resume();
            lapsedZoomFit(5, 0);
        }
        
        //Div element for the tooltip
        const toolTipDiv = d3.select(network).append("div")	
            .attr("class", "network-tooltip")				
            .style("opacity", 0);

        const tree = d3.tree()
            .size([height, width])
            .nodeSize([39, 10]);

        const svg = d3.select(this.element)
            .select('svg')
            .attr('preserveAspectRatio',"xMidYMid meet")
            .call(zoom);

        const g = svg.append("g")

        const collapse = (d) => {
            if (d.children) {
                d._children = d.children;
                d._children.forEach(collapse);
                d.children = null;
            }
        }

        d3.select(window).on('resize', resize);
        
        let i = 0,
            root;
        const duration = 750;
        root = d3.hierarchy(treeData, function(d) { return d.children; });
        root.x0 = width / 2;
        root.y0 = 0;

        // Collapse after the second level
        root.children.forEach(collapse);        

        const update = (source) => {
            const _this = this;
            // Creates a curved (diagonal) path from parent to the child nodes
            const diagonal = (d) => {
                return "M" + (margin.left+(d.x+(width/2))) + "," + (margin.top + d.y)
                    + "C" + (margin.left+(d.x+(width/2))) + "," + ((margin.top + d.y) + (margin.top  + d.parent.y)) / 2
                    + " " + (margin.left+((d.parent.x+(width/2)))) + "," +  ((margin.top + d.y) + (margin.top  + d.parent.y)) / 2
                    + " " + (margin.left+((d.parent.x+(width/2)))) + "," + (margin.top  + d.parent.y);
            }
            // Assigns the x and y position for the nodes
            const treeData = tree(root);
            // Compute the new tree layout.
            const nodes = treeData.descendants(),
                links = treeData.descendants().slice(1);

            // Normalize for fixed-depth.
            nodes.forEach((d) => { d.y = d.depth * 180});

            // Update the nodes…
            var node = g.selectAll('g.node')
                .data(nodes, (d) => {return d.id || (d.id = ++i); });

            // Toggle children on click.
            const click = (d, elementIndex) => {

            }

            // Enter any new nodes at the parent's previous position.
            var nodeEnter = node.enter().append("g")
                .attr("data-id",(d, i)=> {
                    return i;
                })
                .attr("class",(d)=> {
                    return `node node-${d.data.className}`
                })
                .attr("transform", function(d) {
                    return "translate(" + source.y0 + "," + source.x0 + ")";
                });    

            nodeEnter.append("circle")
                .attr("r", 10)
                .style("fill", function(d) {
                    return d._children ? "lightsteelblue" : "#fff"; 
                })
                .attr('class', 'node-transaction')
        
            nodeEnter.append("text")
                .attr("x", function(d) { return -25 })
                .attr("y", function(d) { 
                    return d.children ? 0 : 20;  
                })
                .attr("dy", ".35em")
                .attr("text-anchor", function(d) {
                    return d.children || d._children ? "end" : "start"; 
                })
                .text(function(d) { 
                    if(!d.parent) {
                        return d.data.name
                    } else {
                        return new Date(d.data.timestamp).toLocaleDateString();
                    ;}
                })
                .style("fill-opacity", 1e-6);

            nodeEnter.on("mouseover", function(d) {		
                toolTipDiv.transition()		
                .duration(200)		
                .style("opacity", .9);		
                toolTipDiv.html(`${d.data.start}`)
                .style("left", (d3.event.pageX) + "px")		
                .style("top", (d3.event.pageY - 28) + "px");	
            })					
            .on("mouseout", function(d) {		
                toolTipDiv.transition()		
                    .duration(500)		
                    .style("opacity", 0);	
            })
            .on("click", (d, i, nodes) => {
                    nodes.forEach((n)=>{
                        if(n.classList.contains('selected')){
                            n.classList.remove('selected');
                        }
                    })
                    nodes[i].classList.add('selected');
                    _this.controller.controller(i, _this.constructor.name);
                    if (d.children) {
                        d._children = d.children;
                        d.children = null;
                    } else {
                        d.children = d._children;
                        d._children = null;
                    }
                    update(d);  
                });
          

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) { 
                return "translate(" + d.x + "," + d.y + ")"; 
                });

        nodeUpdate.select("circle")
            .attr("r", 10)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff"; 
                })
        
        // UPDATE
        var nodeUpdate = nodeEnter.merge(node);

        // Transition to the proper position for the node
        nodeUpdate.transition()
        .duration(duration)
        .attr("transform", function(d) { 
            return "translate(" + (margin.left+(d.x+(width/2))) + "," + (margin.top + d.y) + ")";
        });

        // Update the node attributes and style
        nodeUpdate.select('circle.node')
        .attr('r', 10)
        .style("fill", function(d) {
            return d._children ? "lightsteelblue" : "#fff";
        })
        .attr('cursor', 'pointer');

        // Update the node attributes and style
        nodeUpdate.select('text')
        .attr('r', 10)
        .style('fill-opacity', '1');


        // Remove any exiting nodes
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        // On exit reduce the node circles size to 0
        nodeExit.select('circle')
        .attr('r', 1e-6);

        // On exit reduce the opacity of text labels
        nodeExit.select('text')
        .style('fill-opacity', 1e-6);

        // ****************** links section ***************************

        // Update the links...
        var link = g.selectAll('path.link')
            .data(links, function(d) { return d.id; });

        // Enter any new links at the parent's previous position.
        var linkEnter = link.enter().insert('path', "g")
            .attr("class", "link")
            .attr('d', function(d){
                var o = {x: source.x0, y: source.y0};
            return diagonal(d)
            });

        // UPDATE
        var linkUpdate = linkEnter.merge(link);

        // Transition back to the parent element position
        linkUpdate.transition()
            .duration(duration)
            .attr('d', (d) => { 
                    //var o = {x: d.x, y: d.y}
                    //var o2 = {x: d.parent.x+(width), y: d.parent.y}
                    return diagonal(d) 
                });

        // Remove any exiting links
        var linkExit = link.exit().transition()
            .duration(duration)
            .attr('d', function(d) {
            //var o = {x: source.x, y: source.y}
            return diagonal(d)
            })
            .remove();

        // Store the old positions for transition.
        nodes.forEach(function(d){
        d.x0 = d.x;
        d.y0 = d.y;
        });
                
        }
        update(root);

                }
            }  