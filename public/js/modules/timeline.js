export default class TimeLine{
    constructor(elementId,adrr) {
        this.addr = adrr;
        this.element = document.getElementById(elementId);
        };
    
    //const addr = '1Ez69SnzzmePmZX3WpEzMKTrcBF2gpNQ55';

    // Called when the Visualization API is loaded.
    draw(txs) {
        var data;
        var lastEventId = 1;
        var prevSelectedDotDiv = null;
        var eventToItemMapping = {};
        // specify options
        var options = {
            'width':  '100%',
            'height': '125px',
            'start': txs[txs.length-1].start,
            'end': txs[0].start,
            'cluster': true,
            'clusterMaxItems': 1
        };

        // Draw our timeline with the created data and options
        this.timeline = new links.Timeline(this.element, options); 
        this.timeline.draw(txs);
        txs.forEach((n) => {
            let fromTransactions ='';
            let toTransactions ='';
            let type;
            for(const tx in n.from){
                if(!type && n.from[tx].prev_out.addr === this.addr) {
                    type = 'from';
                    return;
                }
            };
            
            for(const tx in n.to){
                if(!type && n.to[tx].addr === this.addr) {
                        type = 'to';
                        return;
                }
            };
        })
    }
    
    /**
     * Adjust the visible time range such that all events are visible.
     */
     adjustVisibleTimeRange(date) {
        let t = new Date(date);
        t = t.setDate(t.getDate() +0.1)

        let y = new Date(date);
        y = y.setDate(y.getDate() -0.1)

        this.timeline.setVisibleChartRange(y, t);
    }

    bindEventHoverEvent() {
        $('.event').on("mouseenter", function(event){
           $('.timeline-event-dot').removeClass("hovered");
           var id = $(this).attr('id').replace(/[^\d]+/img, '');
           eventToItemMapping[id].forEach(function(el) {
             $(el).addClass('hovered');
           });
        });

        $('.event').on("mouseleave", function(event){
           var id = $(this).attr('id').replace(/[^\d]+/img, '');
           eventToItemMapping[id].forEach(function(el) {
             $(el).removeClass('hovered');
           });
        });            
    }

    mouseoverItemEventCallback(eventId){
       $('.event').removeClass("highlighted");
       $('#event_id_'+eventId).addClass("highlighted");
    }

    clickItemEventCallback(eventId, dotDiv){
         $('.timeline-event-dot').removeClass("selected");
         if($(prevSelectedDotDiv)){
            $(prevSelectedDotDiv).removeClass("selected");
         }
         $(dotDiv).addClass("selected");
         prevSelectedDotDiv = dotDiv;
         $('.events').scrollTo('#event_id_'+ eventId, { duration : 'slow' });
    }

    itemToEventMapCallback(eventId, dotDiv, eventIds){
        eventIds.push(eventId);
        eventIds.forEach(function(evtId){

        if(eventToItemMapping.hasOwnProperty(evtId)) {
            eventToItemMapping[evtId].push(dotDiv);
        } else {
            eventToItemMapping[evtId] = [dotDiv];
        }

        });
    }

    itemArialLabelCallback(divDot, className, eventDate, content) {
        //var weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        //monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    
        var eventType = (className == 'green') ? "Ok" :
                         (
                            (className == 'green-m') ? "Maintenance" :
                               (
                                (className == 'yellow') ? "Warning" : "Error"
                            )
                        );
        //var formattedDate = monthNames[eventDate.getMonth()] + " " + eventDate.getDate() + " " + eventDate.getFullYear();
        var dd = eventDate.getDate();
        var mm = eventDate.getMonth()+1; 
        var yyyy = eventDate.getFullYear();
        if(dd < 10) {
            dd='0' + dd
        } 
        if(mm < 10) {
            mm='0'+mm
        } 
        var formattedDate = dd+'/'+mm+'/'+yyyy;
        divDot.setAttribute("aria-label", "Event type " + eventType + " on " +  formattedDate);
    }
    /**
     * Zoom
     * @param zoomVal
     */
    zoom(zoomVal) {
        timeline.zoom(zoomVal);
        timeline.trigger("rangechange");
        timeline.trigger("rangechanged");
    }

    

    /**
     * Adjust the visible time range such that all events are visible.
     */
    adjustVisibleTimeRangeToAccommodateAllEvents() {
        timeline.setVisibleChartRangeAuto();
    }

    /**
     * Move
     * @param moveVal
     */
    move(moveVal) {
        timeline.move(moveVal);
        timeline.trigger("rangechange");
        timeline.trigger("rangechanged");
    }

    /**
     * Move the visible range such that the current time is located in the center of the timeline.
     */
    moveToCurrentTime() {
        timeline.setVisibleChartRangeNow();
    }
} 